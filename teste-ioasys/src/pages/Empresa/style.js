import styled from "styled-components";
import { Navbar } from "../../components/common/Padrao";

export const ContainerEmpresaDetalhe = styled.div``;

export const NavbarEmpresaDetalhe = styled(Navbar)`
  width: auto;
  justify-content: flex-start;
  padding-left: 8rem;
  svg {
    color: #fff;
    margin-right: 2rem;
    width: 2.188rem !important;
    height: 2.188rem;
    cursor: pointer;
  }
  h3 {
    font-size: 2.125rem;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #fff;
  }
  @media screen and (min-width: 320px) and (max-width: 600px) {
    padding-left: 1rem;
  }
  @media screen and (min-width: 600px) and (max-width: 960px) {
    padding-left: 1rem;
  }
`;

export const EmpresaDetalhe = styled.div`
  padding: 8rem;
  .container-empresa {
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: #fff;
    padding: 1rem;
    padding-top: 3rem;
    img {
      width: 48.5rem;
      height: 18.438rem;
    }
    h3 {
      font-size: 2.125rem;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: normal;
      color: #8d8c8c;
      max-width: 48.5rem;
    }
  }
  @media screen and (min-width: 320px) and (max-width: 600px) {
    padding: 1rem;
    img {
      width: 100% !important;
      height: auto !important;
    }
  }
  @media screen and (min-width: 600px) and (max-width: 960px) {
    padding: 1rem;
    img {
      width: 100% !important;
      height: auto !important;
    }
  }
`;
