import React, { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import {
  ContainerEmpresaDetalhe,
  NavbarEmpresaDetalhe,
  EmpresaDetalhe,
} from "./style";
import { useDispatch, useSelector } from "react-redux";
import { Creators as empresaActions } from "../../store/reducers/empresas";
import { useParams } from "react-router-dom";

function Empresa({history}) {
  const empresa = useSelector((state) => state.reducers.empresas.empresa);
  const dispatch = useDispatch();
  const {id} = useParams();

  useEffect(() => {
    dispatch(empresaActions.empresaRequest({ id: id }));
  }, []);

  return (
    <ContainerEmpresaDetalhe>
        <NavbarEmpresaDetalhe>
        <FontAwesomeIcon icon={faArrowLeft} onClick={() => history.push("/Home")}/>
        <h3>{empresa.data.enterprise.enterprise_name}</h3>
      </NavbarEmpresaDetalhe>
      <EmpresaDetalhe>
        <div className="container-empresa">
          <img
            src={`https://empresas.ioasys.com.br/${empresa.data.enterprise.photo}`}
            alt=""
          />
          <h3>{empresa.data.enterprise.description}</h3>
        </div>
      </EmpresaDetalhe>
      {empresa.isLoading && <h1>Carregando</h1>}
    </ContainerEmpresaDetalhe>
  );
}

export default Empresa;
