import styled from "styled-components";
import { Input , Navbar } from "../../components/common/Padrao";

export const ContainerHomePage = styled.div`
  overflow-x: hidden;
`;

export const NavbarHome = styled(Navbar)`
  .container-input-navbar {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    width: 100%;
    position: relative;
    transition: all 0.6s ease-in-out;
    animation: show 0.6s;
    max-width: 700px;
    input::placeholder {
      color: #fff;
      text-align: center;
    }
    img:first-child {
      width: 3.75rem;
      height: 3.75rem;
      position: absolute;
      left: 0;
      top: -24px;
    }
    img {
      width: 3.75rem;
      height: 3.75rem;
      position: absolute;
      right: 0;
      top: -22px;
    }
  }

  .logo-nav {
    width: 14.64rem;
    height: 3.563rem;
  }
  .icon-search {
    width: 3.75rem;
    height: 3.75rem;
    position: absolute;
    right: 3rem;
  }

  @media screen and (min-width: 320px) and (max-width: 600px) {
    justify-content: space-between;
    padding: 1rem;
    .container-input-navbar {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
      margin-right: 1rem;
      img:first-child {
        position: initial;
      }
      img {
        position: initial;
      }
    }
  }
  @media screen and (min-width: 320px) and (max-width: 600px) {
      .container-input-navbar{
          margin-right: 0;
      }
  }
`;

export const ContainerConteudo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 8rem;
  .msg-inicial {
    padding: 10rem;
  }
  h1 {
    font-size: 2rem;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.22;
    letter-spacing: -0.45px;
    text-align: center;
    color: #383743;
  }

  @media screen and (min-width: 320px) and (max-width: 600px) {
    padding: 2rem;
    h1 {
      width: 100%;
    }
  }
  @media screen and (min-width: 600px) and (max-width: 960px) {
    padding: 2rem;
    h1 {
      width: 100%;
    }
  }
  @keyframes show {
    from {
      opacity: 0;
    }
    to {
      oapcity: 1;
    }
  }
`;

export const InputNavbar = styled(Input)`
  color: #fff;
  border-bottom: 1px solid #fff;
  width: 100%;
  padding-left: 4rem;
  padding-right: 4rem;
`;

export const ContainerEmpresa = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  background-color: #fff;
  padding: 1rem;
  width: 100%;
  margin-bottom: 2rem;
  transition: all 0.5s;
  animation: show 0.5s;
  cursor: pointer;

  .img-enterprise {
    width: 440px;
    height: 240px;
    img{
        width: 100%;
    }
  }

  .empresa-infos {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: space-around;
    margin-left: 2rem;
    list-style: none;
    li {
      font-size: 1.5rem;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #8d8c8c;
    }
    li:first-child {
      font-size: 1.875rem;
      font-weight: bold;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #1a0e49;
    }
    li:last-child {
      font-size: 1.125rem;
      font-weight: normal;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: left;
      color: #8d8c8c;
    }
  }
  @media screen and (min-width: 320px) and (max-width: 600px) {
    flex-direction: column;
    .img-enterprise{
        width: 100%;
    }
    .empresa-infos{
        margin-left: 0;
    }
  }
  @media screen and (min-width: 600px) and (max-width: 960px) {
    flex-direction: row;
    .img-enterprise{
        width: 18.313rem;
        height: 10rem;
    }
  }
`;
