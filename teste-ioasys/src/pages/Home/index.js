import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  ContainerHomePage,
  NavbarHome,
  ContainerConteudo,
  InputNavbar,
  ContainerEmpresa,
} from "./style";
import { Creators as empresasActions } from "../../store/reducers/empresas";

import LogoIoasys from "../../content/images/logo-nav.png";
import IconSearch from "../../content/icons/ic-search-copy.svg";
import IconClose from "../../content/icons/ic-close.svg";

function Home({history}) {
  const [openInput, setOpenInput] = useState(false);
  const [search, setSearch] = useState("");
  const empresas = useSelector(
    (state) => state.reducers.empresas.listEmpresas
  );
  const {data: {enterprises}} = empresas;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(empresasActions.empresasRequest());
  }, [empresasActions]);

  const accessEnterprise = (id) => {
    dispatch(empresasActions.empresaRequest({id: id}));
    history.push(`/Empresa/${id}`);
  }

  return (
    <ContainerHomePage>
      <NavbarHome>
        {openInput ? (
          <div className="container-input-navbar">
            <img src={IconSearch} />
            <InputNavbar
              placeholder="Pesquisar empresa"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            ></InputNavbar>
            <img src={IconClose} onClick={() => setOpenInput(!openInput)} />
          </div>
        ) : (
          <>
            <img src={LogoIoasys} alt="logo-ioasys" className="logo-nav" />
            <img
              src={IconSearch}
              alt="icon-search"
              className="icon-search"
              onClick={() => setOpenInput(!openInput)}
            />
          </>
        )}
      </NavbarHome>
      <ContainerConteudo>
        {search !== "" ? (
          enterprises
            .filter((val) => {
              if (search === "" || search === null || search === undefined) {
                return val;
              } else if (
                val.enterprise_name.toLowerCase().includes(search.toLowerCase())
              ) {
                return val;
              }
            })
            .map((empresa) => {
              return (
                <ContainerEmpresa key={empresa.id} onClick={() => accessEnterprise(empresa.id.toString())}> 
                  <div className="img-enterprise">
                    <img
                      src={`https://empresas.ioasys.com.br/${empresa.photo}`}
                      alt="img-empresa"
                    />
                  </div>
                  <div className="empresa-infos">
                    <li>{empresa.enterprise_name}</li>
                    <li>{empresa.enterprise_type.enterprise_type_name}</li>
                    <li>{empresa.country}</li>
                  </div>
                </ContainerEmpresa>
              );
            })
        ) : (
          <h1 className="msg-inicial">Use o icone de pesquisa para achar uma empresa</h1>
        )}
      </ContainerConteudo>
    </ContainerHomePage>
);
}

export default Home;
