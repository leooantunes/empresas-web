import styled from "styled-components";

export const ContainerPageLogin = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: auto;
  background-color: #eeecdb;
  overflow: hidden;
`;

export const ContainerLogin = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 5rem;
  .error-span {
    color: red;
  }
  .logo-ioasys {
    width: 18.438rem;
    height: 4.5rem;
  }
  h3 {
    font-size: 1.6rem;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: -1.2px;
    text-align: center;
    width: 10.75rem;
    height: 3.75rem;
  }
  p {
    font-size: 1.063rem;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.48;
    letter-spacing: 0.2px;
    text-align: center;
    width: 21.938rem;
    height: 3.125rem;
    margin: 1.563rem 0.25rem 2.875rem 0;
  }
  .container-inputs {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    .input-email {
      display: flex;
      flex-direction: row;
      justify-content: flex-start;
      margin-bottom: 1rem;
      width: 100%;
      position: relative;
      input {
        padding-left: 2rem;
        text-align: left;
        font-size: 1.36rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: -0.31px;
        color: #383743;
        ::placeholder {
          color: #383743;
          font-size: 1.125rem;
          font-weight: normal;
          font-stretch: normal;
          font-style: normal;
          line-height: normal;
          letter-spacing: -0.25px;
          text-align: left;
        }
      }
    }
    .input-senha {
      display: flex;
      flex-direction: row;
      justify-content: flex-start;
      margin-bottom: 1rem;
      width: 100%;
      position: relative;
      input {
        padding-left: 2rem;
        text-align: left;
        padding-left: 2rem;
        text-align: left;
        font-size: 1.36rem;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: -0.31px;
        color: #383743;
        ::placeholder {
          color: #383743;
          font-size: 1.125rem;
          font-weight: normal;
          font-stretch: normal;
          font-style: normal;
          line-height: normal;
          letter-spacing: -0.25px;
          text-align: left;
        }
      }
    }
    img {
      position: absolute;
      left: 0;
    }
  }
`;
