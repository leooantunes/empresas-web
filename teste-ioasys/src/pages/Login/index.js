import React, { useState } from "react";
import { css } from "@emotion/core";
import { Redirect } from "react-router-dom";
import { Button, Input, ButtonDisable } from "../../components/common/Padrao";
import { ContainerPageLogin, ContainerLogin } from "./style";
import { useDispatch, useSelector } from "react-redux";
import { Creators as loginActions } from "../../store/reducers/user";

import LogoIoasys from "../../content/images/logo-home.png";
import IconCadeado from "../../content/icons/ic-cadeado.svg";
import IconEmail from "../../content/icons/ic-email.svg";
import CircleLoader from "react-spinners/PuffLoader";

const propsSpinner = css`
  top: 35px !important;
  right: 25px !important;
`;

function Login() {
  //   const { isLoading, error, data } = login;
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { user } = useSelector((store) => store.reducers);
  const dispatch = useDispatch();

  const handleLogin = (e) => {
    e.preventDefault();
    dispatch(loginActions.loginRequest({ email: username, password }));
  };

  return (
    <ContainerPageLogin>
      <ContainerLogin>
        <img src={LogoIoasys} alt="logo" className="logo-ioasys" />
        <h3>BEM-VINDO AO EMPRESAS</h3>
        <p>
          Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </p>
        <form className="container-inputs">
          <div className="input-email">
            <img src={IconEmail} alt="icon-email" />
            <Input
              id="user-email"
              type="text"
              placeholder="Email"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              color={user.error ? 'error' : 'sucess'}
            />
          </div>
          <div className="input-senha">
            <img src={IconCadeado} alt="icon-cadeado" />
            <Input
              id="user-password"
              type="password"
              placeholder="Senha"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              color={user.error ? 'error' : 'sucess'}
            />
          </div>
          {user.error && (
            <span className="error-span">Email ou senha incorretos</span>
          )}
          {user.isLoading ? (
            <ButtonDisable>Entrar</ButtonDisable>
          ) : (
            <Button type="submit" onClick={(e) => handleLogin(e)}>
              Entrar
            </Button>
          )}
          {user.isLoading ? (
            <div className="back-spinner">
              <CircleLoader color="#57BBBC" size="35" css={propsSpinner} />
            </div>
          ) : (
            ""
          )}
          {!!user.data.investor.id && <Redirect to="/Home" />}
        </form>
      </ContainerLogin>
    </ContainerPageLogin>
  );
}

export default Login;
