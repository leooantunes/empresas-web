import { all, takeLatest } from 'redux-saga/effects'

import { types as loginTypes } from '../reducers/user';
import { types as empresasTypes } from '../reducers/empresas';
import { login } from './user';
import { getEmpresas, getEmpresa } from './empresa';

export default function* rootSaga() {
  yield all([
    takeLatest(loginTypes.LOGIN_REQUEST, login),
    takeLatest(empresasTypes.EMPRESAS_REQUEST, getEmpresas),
    takeLatest(empresasTypes.EMPRESA_REQUEST, getEmpresa),
  ]);
}