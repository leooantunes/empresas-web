import { call, put } from 'redux-saga/effects';
import api from '../../services/api';

import { Creators as loginActions } from '../reducers/user';

export function* login(action) {
  try {

    const body = {
      email: action.payload.email,
      password: action.payload.password
    }

    const response = yield call(api.post, "users/auth/sign_in", body);
    
    localStorage.setItem("access-token", response.headers["access-token"]);
    localStorage.setItem("client", response.headers["client"]);
    localStorage.setItem("token_type", response.headers["token-type"]);
    localStorage.setItem("uid", response.headers["uid"]);

    yield put(loginActions.loginSuccess(response.data));
  } catch (err) {
    yield put(loginActions.loginFail('Email ou senha incorretos'));
  }
}