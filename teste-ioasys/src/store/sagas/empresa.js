import { call,put } from 'redux-saga/effects';
import api from '../../services/api';

import { Creators as empresasActions } from '../reducers/empresas';

export function* getEmpresas(){
    try {
        const response = yield call(api.get, "enterprises", {
            headers: {
                'uid': localStorage.getItem("uid"),
                'client': localStorage.getItem("client"),
                'access-token': localStorage.getItem("access-token")
            }
        });
        yield put(empresasActions.empresasSuccess(response.data));
    } catch (error) {
        yield put(empresasActions.empresasFail('Sem acesso as empresas'));
    }
}

export function* getEmpresa(action){
    try {
        const data = {id: action.payload.id};
        const response = yield call(api.get,`enterprises/${data.id}`,  {
            headers: {
                'access-token': localStorage.getItem("access-token"),
                'client': localStorage.getItem("client"),
                'uid': localStorage.getItem("uid")
            }
        });
        yield put(empresasActions.empresaSuccess(response.data));
        localStorage.setItem("id", response.data.enterprise.id);
    } catch (error) {
        yield put(empresasActions.empresaFail('Sem acesso as empresas'));
    }
}