// Types
export const types = {
  LOGIN_REQUEST: "user/LOGIN_REQUEST",
  LOGIN_SUCCESS: "user/LOGIN_SUCCESS",
  LOGIN_FAIL: "user/LOGIN_FAIL",
};

const INITIAL_STATE = {
  isLoading: false,
  data: {
    investor: { id: "" },
  },
  error: null,
};

export default function login(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        data: action.payload.data,
      };
    case types.LOGIN_FAIL:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
}

export const Creators = {
  loginRequest: ({email, password}) => ({
    type: types.LOGIN_REQUEST,
    payload: { email, password },
  }),
  loginSuccess: (data) => ({
    type: types.LOGIN_SUCCESS,
    payload: { data },
  }),
  loginFail: (error) => ({
    type: types.LOGIN_FAIL,
    payload: { error },
  }),
};
