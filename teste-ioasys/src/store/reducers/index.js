import {combineReducers} from 'redux';

import user from './user';
import empresas from './empresas';

export default combineReducers({
    user,
    empresas
})