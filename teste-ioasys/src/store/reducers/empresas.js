// Types
export const types = {
  EMPRESAS_REQUEST: "empresas/EMPRESAS_REQUEST",
  EMPRESAS_SUCCESS: "empresas/EMPRESAS_SUCCESS",
  EMPRESAS_FAIL: "empresas/EMPRESAS_FAIL",
  EMPRESA_REQUEST: "empresa/EMPRESA_REQUEST",
  EMPRESA_SUCCESS: "empresa/EMPRESA_SUCCESS",
  EMPRESA_FAIL: "empresa/EMPRESA_FAIL",
};

const INITIAL_STATE = {
  listEmpresas: {
    isLoading: false,
    data: {
      enterprises: [],
    },
    error: null,
  },
  empresa: {
    isLoading: false,
    data: {
      enterprise: {id: 1}
    },
    error: null,
  },
};

export default function empresas(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.EMPRESAS_REQUEST:
      return {
        ...state,
        listEmpresas: { isLoading: true, ...state.listEmpresas },
      };
    case types.EMPRESAS_SUCCESS:
      return {
        ...state,
        listEmpresas: {
          isLoading: false,
          error: null,
          data: action.payload.data,
        },
      };
    case types.EMPRESAS_FAIL:
      return {
        ...state,
        listEmpresas: {
          isLoading: false,
          error: action.payload.error,
          ...state.listEmpresas,
        },
      };
    case types.EMPRESA_REQUEST:
      return {
        ...state,
        empresa: {
          isLoading: true,
          ...state.empresa,
        },
      };
    case types.EMPRESA_SUCCESS:
      return {
        ...state,
        empresa: { isLoading: false, error: null, data: action.payload.data},
      };
    case types.EMPRESA_FAIL:
      return {
        ...state,
        empresa: {
          isLoading: false,
          error: action.payload.error,
          ...state.empresa,
        },
      };
    default:
      return state;
  }
}

export const Creators = {
  empresasRequest: () => ({
    type: types.EMPRESAS_REQUEST,
  }),
  empresasSuccess: (data) => ({
    type: types.EMPRESAS_SUCCESS,
    payload: { data },
  }),
  empresasFail: (error) => ({
    type: types.EMPRESAS_FAIL,
    payload: { error },
  }),
  empresaRequest: ({ id }) => ({
    type: types.EMPRESA_REQUEST,
    payload: { id },
  }),
  empresaSuccess: (data) => ({
    type: types.EMPRESA_SUCCESS,
    payload: { data },
  }),
  empresaFail: (error) => ({
    type: types.EMPRESA_FAIL,
    payload: { error },
  }),
};
