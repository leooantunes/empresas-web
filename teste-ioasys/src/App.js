import React from "react";
import { Provider } from "react-redux";
import {Router} from 'react-router-dom';
import {history} from './store/helper/history';
import GlobalStyle from "./content/styles/styleGlobal";
import Store from "./store";
import Routes from "./routes/routes";

function App() {
  return (
    <div className="App">
      <Provider store={Store}>
        <Router history={history}>
          <GlobalStyle />
          <Routes />
        </Router>
      </Provider>
    </div>
  );
}

export default App;
