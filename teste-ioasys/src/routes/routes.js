import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { PrivateRoute } from "../components/PrivateRoute";

import Login from "../pages/Login";
import Home from "../pages/Home";
import Empresa from "../pages/Empresa";

function routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <PrivateRoute>
          <Route exact path="/Home" component={Home} />
          <Route exact path="/Empresa/:id" component={Empresa} />
        </PrivateRoute>
      </Switch>
    </BrowserRouter>
  );
}

export default routes;
