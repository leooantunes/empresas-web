import React from 'react';
import {storiesOf} from '@storybook/react'
import {Button, ButtonDisable} from '../components/common/Padrao';

storiesOf('Button', module).add('Default', () => <Button>Button</Button>);

storiesOf('Button', module).add('Disable', () => <ButtonDisable>Button</ButtonDisable>);
