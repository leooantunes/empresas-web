import styled from "styled-components";

const handleColorType = (color) => {
  switch (color) {
    case "error":
      return "red";
    case "sucess":
      return "#000";
    default:
      return "#000";
  }
};

export const Input = styled.input`
  border: none;
  outline: none;
  background-color: transparent;
  font-size: 1.369rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: -0.31px;
  text-align: center;
  border-bottom: 1px solid ${({ color }) => handleColorType(color)};
  width: 100%;
  ::placeholder {
    color: #000;
    text-align: left;
  }
`;

export const Button = styled.button`
  outline: none;
  border: none;
  width: 100%;
  margin: 2.563rem 0 0;
  padding: 1.125rem 8.375rem 1rem 8.438rem;
  border-radius: 3.9px;
  background-color: #57bbbc;
  color: #fff;
  text-transform: uppercase;
  font-weight: bold;
  cursor: pointer;
`;

export const ButtonDisable = styled.button`
  outline: none;
  border: none;
  width: 100%;
  margin: 2.563rem 0 0;
  padding: 1.125rem 8.375rem 1rem 8.438rem;
  border-radius: 3.9px;
  background-color: grey;
  color: #fff;
  text-transform: uppercase;
  pointer-events: none;
  font-weight: bold;
  cursor: pointer;
`;

export const Navbar = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: #ee4c77;
  width: 100%;
  position: relative;
  padding: 1rem;
  transition: all 1s ease-in-out;
  animation: show 1s;
  color: #fff;
`;
